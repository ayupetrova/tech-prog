# пункт 2 - проверка корректности функции

from main import min_number, max_number, sum_array, multiplication, read_file, store_integer
from math import prod
import pytest


@pytest.fixture
def return_data():
    data = read_file('nums.txt')
    return data


def test_min_number(return_data):
    assert store_integer(min_number(return_data)) ==\
           store_integer(min(return_data))


def test_max_number(return_data):
    assert store_integer(max_number(return_data)) ==\
           store_integer(max(return_data))


def test_summa(return_data):
    assert store_integer(sum_array(return_data)) ==\
           store_integer(sum(return_data))


def test_multiplication(return_data):
    assert store_integer(multiplication(return_data)) ==\
           store_integer(prod(return_data))
