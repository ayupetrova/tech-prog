# пункт 3 - тестирование скорости

from main import min_number, max_number, sum_array, multiplication, store_integer
from additionals import random_array
from datetime import datetime
import pytest


@pytest.fixture
def create_arrays():
    array_sm = random_array(10, -10, 10)
    array_l = random_array(10000, -1000, 10000)
    return [array_sm, array_l]


def test_speed_min(create_arrays):
    start = datetime.now()
    store_integer(min_number(create_arrays[0]))
    end_1 = datetime.now()
    store_integer(min_number(create_arrays[1]))
    end_2 = datetime.now()
    assert (end_1 - start) < (end_2 - end_1)


def test_speed_max(create_arrays):
    start = datetime.now()
    store_integer(max_number(create_arrays[0]))
    end_1 = datetime.now()
    store_integer(max_number(create_arrays[1]))
    end_2 = datetime.now()
    assert (end_1 - start) < (end_2 - end_1)


def test_speed_mult(create_arrays):
    start = datetime.now()
    store_integer(multiplication(create_arrays[0]))
    end_1 = datetime.now()
    store_integer(multiplication(create_arrays[1]))
    end_2 = datetime.now()
    assert (end_1 - start) < (end_2 - end_1)


def test_speed_sum(create_arrays):
    start = datetime.now()
    store_integer(sum_array(create_arrays[0]))
    end_1 = datetime.now()
    store_integer(sum_array(create_arrays[1]))
    end_2 = datetime.now()
    assert (end_1 - start) < (end_2 - end_1)
