# пункт 4 - стресс тестирование

from main import min_number, max_number, sum_array, multiplication, store_integer
from additionals import random_array
from math import prod
import pytest


@pytest.fixture()
def return_data():
    data = random_array(1000, -1000, 10001)
    return data


def test_min_stress(return_data):
    for i in range(1001):
        assert store_integer(min_number(return_data)) == \
               store_integer(min(return_data))


def test_max_stress(return_data):
    for i in range(1001):
        assert store_integer(max_number(return_data)) == \
               store_integer(max(return_data))


def test_mult_stress(return_data):
    for i in range(1001):
        assert store_integer(multiplication(return_data)) ==\
               store_integer(prod(return_data))


def test_sum_stress(return_data):
    for i in range(1001):
        assert store_integer(sum_array(return_data)) == \
               store_integer(sum(return_data))
