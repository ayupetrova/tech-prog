# пункт 1 - реализация функций
# пункт 5


def read_file(filename):
    with open(filename, 'r', encoding='utf-8') as file:
        lines = file.read().splitlines()
        data = []
        for line in lines:
            line_items = line.split(sep=' ')
            data.extend(line_items)
            data = list(map(int, data))
        return data


def min_number(array):
    curr_min_num = array[0]
    for i in range(1, len(array)):
        if curr_min_num > array[i]:
            curr_min_num = array[i]
    return curr_min_num


def max_number(array):
    curr_max_num = array[0]
    for i in range(1, len(array)):
        if array[i] > curr_max_num:
            curr_max_num = array[i]
    return curr_max_num


def sum_array(array):
    total_sum = 0
    for i in array:
        total_sum += i
    return total_sum


def multiplication(array):
    total_mult = 1
    for i in array:
        total_mult *= i
    return total_mult


def store_integer(value):
    limit = 10000000
    if (abs(value) // limit) > 1:
        return str(value)
    else:
        return value


if __name__ == '__main__':
    nums = read_file('nums.txt')
    result_array = [store_integer(min_number(nums)), store_integer(max_number(nums)),
                    store_integer(sum_array(nums)), store_integer(multiplication(nums))]
    print(f'В файле:', *nums)
    print(f'Минимальное:', result_array[0])
    print(f'Максимальное:', result_array[1])
    print(f'Сумма:', result_array[2])
    print(f'Произведение:', result_array[3])
