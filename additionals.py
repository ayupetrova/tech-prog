from random import randint, seed


def random_array(length, start, end):
    seed(27)
    generated_array = []
    for _ in range(length):
        generated_array.append(randint(start, end))
    return generated_array
